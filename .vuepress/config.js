module.exports = {
    title: 'Ansible Playbooks',
    description: 'Collection of Ansible playbooks and roles.',
    head: [
        ['link', { rel: "icon", type: "image/png", href: "icon.png"}],
    ],
    themeConfig: {
        sidebar: 'auto',
        repo: 'mint-system/ansible-playbooks',
        docsBranch: 'master',
        editLinks: true,
        nav: [
            { text: 'Home', link: '/' }
        ],
    },
    dest: 'public',
}
